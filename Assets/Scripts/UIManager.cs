﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Text playerScoreText;
    public Text enemyscoreText;

    public Image sliderImage;
    private float sliderValue;

	// Use this for initialization
	void Start () {
        RefreshText();
        if (GlobalScore.PlayerScore + GlobalScore.EnemyScore > 0)
        {
            sliderValue = GlobalScore.PlayerScore / (GlobalScore.PlayerScore + GlobalScore.EnemyScore);
        }
        else
        {
            sliderValue = 0.5f;
        }
        sliderImage.fillAmount = sliderValue;
	}

    private void RefreshText()
    {
        playerScoreText.text = GlobalScore.PlayerScore.ToString();
        enemyscoreText.text = GlobalScore.EnemyScore.ToString();
    }
}
