﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private void Start()
    {
        PlayerController.onPlayerDeath += RefreshEnemyScore;
        AIController.onComputerDeath += RefreshPlayerScore;
    }

    private void RefreshEnemyScore()
    {
        GlobalScore.EnemyScore += 1;
        
        PlayerController.onPlayerDeath -= RefreshEnemyScore;
        AIController.onComputerDeath -= RefreshPlayerScore;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void RefreshPlayerScore()
    {
        GlobalScore.PlayerScore += 1;
        
        PlayerController.onPlayerDeath -= RefreshEnemyScore;
        AIController.onComputerDeath -= RefreshPlayerScore;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnDestroy()
    {
        PlayerController.onPlayerDeath -= RefreshEnemyScore;
        AIController.onComputerDeath -= RefreshPlayerScore;
    }
}
