﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed = 10;
    public float turnSpeed = 100;

    public void Move(float value)
    {
        Vector3 movement = transform.forward * value * speed * Time.deltaTime;
        transform.position += movement;
    }

    public void Turn(float value)
    {
        float turn = turnSpeed * Time.deltaTime * value;
        Quaternion turnAngle = Quaternion.Euler(0, turn, 0);
        transform.rotation *= turnAngle;
    }
}
