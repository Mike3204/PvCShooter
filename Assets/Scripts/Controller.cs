﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    protected Shooting shooting;
    protected Movement movement;

    // Use this for initialization
    void Awake () {
        shooting = GetComponent<Shooting>();
        movement = GetComponent<Movement>();
	}
}
