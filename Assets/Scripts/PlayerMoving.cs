﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour {

    public float speed = 5;
    public float turnSpeed = 5;
    private float horizontalInputValue;
    private float verticalInputValue;
    private Shooting shooting;

    // Use this for initialization
    void Start () {
        shooting = GetComponent<Shooting>();
	}
	
	// Update is called once per frame
	void Update () {
        horizontalInputValue = Input.GetAxis("Horizontal");
        verticalInputValue = Input.GetAxis("Vertical");
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            shooting.Shoot();
        }
    }

    private void FixedUpdate()
    {
        Move();
        Turn();
    }

    private void Move()
    {
        Vector3 movement = transform.forward * verticalInputValue * speed * Time.deltaTime;
        transform.position += movement;
    }

    private void Turn()
    {
        float turn = turnSpeed * Time.deltaTime * horizontalInputValue;
        Quaternion turnAngle = Quaternion.Euler(0, turn, 0);
        transform.rotation *= turnAngle;
    }
}
