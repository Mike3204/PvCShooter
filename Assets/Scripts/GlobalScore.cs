﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalScore {
    
    public static float PlayerScore;
    public static float EnemyScore;
}
