﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

    public GameObject bullet;
    private GameObject newBullet;
    public Transform spawnPoint;
    private Rigidbody newBulletRigidbody;
    public float bulletSpeed;
    public float timeBetweenBullets = 0.1f;
    private float timer;

    private void Update()
    {
        timer += Time.deltaTime;
    }

    public void Shoot()
    {
        if (timer <= timeBetweenBullets)
        {
            return;
        }

        newBullet = Instantiate(bullet, spawnPoint.position, spawnPoint.rotation);
        newBulletRigidbody = newBullet.GetComponent<Rigidbody>();
        newBulletRigidbody.AddForce(transform.forward * bulletSpeed, ForceMode.VelocityChange);

        timer = 0;
    }
}
