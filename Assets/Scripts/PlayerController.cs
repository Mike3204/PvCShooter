﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    public static System.Action onPlayerDeath; 

    private float horizontalInputValue;
    private float verticalInputValue;

    private void Update()
    {
        horizontalInputValue = Input.GetAxis("Horizontal");
        verticalInputValue = Input.GetAxis("Vertical");
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            shooting.Shoot();
        }
    }

    private void FixedUpdate()
    {
        movement.Move(verticalInputValue);
        movement.Turn(horizontalInputValue);
    }

    private void OnDisable()
    {
        if (onPlayerDeath != null)
        {
            onPlayerDeath();
        }
    }
}
