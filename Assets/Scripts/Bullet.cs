﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.SetActive(false);
        }
        else
        {
            rigidbody.AddForce(transform.forward * 50, ForceMode.VelocityChange);
        }
    }
}
