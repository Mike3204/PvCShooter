﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

    public float minBorderPoint = -8;
    public float maxBorderPoint = 8;
    private bool avoiding;
    public LayerMask playerMask;
    public Transform raycastPoint;

    public static System.Action onComputerDeath;
	
	// Update is called once per frame
	void Update () {
        if (!avoiding)
        {
            movement.Move(0.8f);
        }
        AIMovement();
        AIShooting();
	}

    private void AIMovement()
    {
        if (
            transform.position.x < minBorderPoint || 
            transform.position.x > maxBorderPoint ||
            transform.position.z < minBorderPoint ||
            transform.position.z > maxBorderPoint ||
            Physics.Raycast(shooting.spawnPoint.position, transform.forward, 1f)
            )
        {
            movement.Move(0.2f);
            movement.Turn(Random.Range(0.5f, 1));
            avoiding = true;
        }
        else
        {
            avoiding = false;
        }
    }

    private void AIShooting()
    {
        if (Physics.Raycast(raycastPoint.position, transform.forward, 50f, playerMask))
        {
            shooting.Shoot();
        }
    }

    private void OnDisable()
    {
        if (onComputerDeath != null)
        {
            onComputerDeath();
        }
    }
}
